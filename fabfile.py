# -*- coding: utf-8 -*-
from fabric.api import *
from fabric.contrib.files import exists, append, contains
from os import path
import os, json, datetime

djangoplus_repository_url = 'git@bitbucket.org:brenokcc/djangoplus.git'
hostname = '177.20.147.203'
username = 'bruno'
bitbucket_username = 'brenokcc'
bitbucket_password = '1793ifrn.?'
project_dir = os.getcwd()
project_name = project_dir.split('/')[-1]
remote_project_dir = '/var/opt/{}'.format(project_name)


GIT_INGORE_FILE_CONTENT = '''*.pyc
.svn
.DS_Store
.DS_Store?
._*
.idea/*
.Spotlight-V100
.Trashes
ehthumbs.db
Thumbs.db
.project
.pydevproject
.settings/*
sqlite.db
media/*
mail/*
'''
NGINEX_FILE_CONTENT = '''server {
    client_max_body_size 100M;
    listen 80;
    server_name %(domain_name)s;
    access_log /var/opt/%(project_name)s/logs/nginx_access.log;
    error_log /var/opt/%(project_name)s/logs/nginx_error.log;
    location /static {
        alias /var/opt/%(project_name)s/static;
    }
    location /media {
        alias /var/opt/%(project_name)s/media;
    }
    location / {
        proxy_pass_header Server;
        proxy_set_header Host $http_host;
        proxy_redirect off;
        proxy_set_header X-Real_IP $remote_addr;
        proxy_set_header X-Scheme $scheme;
        proxy_connect_timeout 600s;
        proxy_send_timeout 600s;
        proxy_read_timeout 600s;
        proxy_pass http://localhost:%(port)s/;
    }
}
'''
SUPERVISOR_FILE_CONTENT = '''[program:%(project_name)s]
directory = /var/opt/%(project_name)s
user = www-data
command = /var/opt/%(project_name)s/gunicorn_start.sh
stdout_logfile = /var/opt/%(project_name)s/logs/supervisor_out.log
stderr_logfile = /var/opt/%(project_name)s/logs/supervisor_err.log
'''
GUNICORN_FILE_CONTENT = '''#!/bin/bash
set -e
source /var/opt/.virtualenvs/%(project_name)s/bin/activate
mkdir -p /var/opt/%(project_name)s/logs
cd /var/opt/%(project_name)s
exec gunicorn %(project_name)s.wsgi:application -w 1 -b 127.0.0.1:%(port)s --timeout=600 --user=www-data --group=www-data --log-level=debug --log-file=/var/opt/%(project_name)s/logs/gunicorn.log 2>>/var/opt/%(project_name)s/logs/gunicorn.log
'''
LIMITS_FILE_CONTENT = '''
*               soft     nofile           65536
*               hard     nofile           65536
root            soft     nofile           65536
root            hard     nofile           65536
'''
BASHRC_FILE_CONTENT = '''
export WORKON_HOME=/var/opt/.virtualenvs
mkdir -p $WORKON_HOME
source /usr/local/bin/virtualenvwrapper.sh
'''


def debug(s):
    print('\n\n\n\n\n[{}] {}'.format(datetime.datetime.now(), s))


def available_port():
    nginex_dir = '/etc/nginx/sites-enabled'
    port = 8000
    with cd(nginex_dir):
        files = run('ls').split()
        files.remove('default')
        if project_name in files:
            files = [project_name]
        if files:
            command = "grep  localhost {} | grep -o '[0-9]*'".format(' '.join(files))
            ports = run(command).split()
            ports.sort()
            port = ports[-1]
            if project_name not in files:
                port = int(port) + 1
    debug('Returning port {}!'.format(port))
    return port


def check_local_keys():
    local_home_dir = local('echo $HOME', capture=True)
    local_ssh_dir = path.join(local_home_dir, '.ssh')
    local_public_key_path = path.join(local_ssh_dir, 'id_rsa.pub')
    if not os.path.exists(local_ssh_dir):
        debug('Creating dir {}...'.format(local_ssh_dir))
        local('mkdir {}'.format(local_ssh_dir))
        if not path.exists(local_public_key_path):
            local("ssh-keygen -f {}/id_rsa -t rsa -N ''".format(local_ssh_dir))

    key = open(local_public_key_path, 'r').read().strip()
    debug('Checking if private key was uploaded to bitbucket...')
    command = 'curl -X GET -u "{}:{}" https://api.bitbucket.org/1.0/users/{}/ssh-keys'.format(bitbucket_username, bitbucket_password, bitbucket_username)
    response = local(command, capture=True)

    if key not in response:
        debug('Uploading private key to bitbucket...')
        command = 'curl -X POST -u "{}:{}" https://api.bitbucket.org/1.0/users/{}/ssh-keys --data-urlencode "key={}"'.format(bitbucket_username, bitbucket_password, bitbucket_username, key)
        response = local(command, capture=True)
        # print response


def check_remote_keys():
    local_home_dir = local('echo $HOME', capture=True)
    local_ssh_dir = path.join(local_home_dir, '.ssh')
    local_public_key_path = path.join(local_ssh_dir, 'id_rsa.pub')
    local_private_key_path = path.join(local_ssh_dir, 'id_rsa')

    remote_home_dir = run('echo $HOME')
    remote_ssh_dir = path.join(remote_home_dir, '.ssh')
    remote_public_key_path = path.join(remote_ssh_dir, 'id_rsa.pub')
    remote_private_key_path = path.join(remote_ssh_dir, 'id_rsa')
    remote_private_known_hosts_path = path.join(remote_ssh_dir, 'known_hosts')
    if not exists(remote_ssh_dir):
        debug('Creading remote dir {}...'.format(remote_ssh_dir))
        run('mkdir -p {}'.format(remote_ssh_dir))
        debug('Creating empty file {}...'.format(remote_private_known_hosts_path))
        run('touch {}'.format(remote_private_known_hosts_path))

    with cd(remote_ssh_dir):
        public_key = open(local_public_key_path, 'r').read()
        private_key = open(local_private_key_path, 'r').read()
        debug('Checking if public key is in file {}...'.format(remote_public_key_path))
        if not contains(remote_public_key_path, public_key):
            debug('Appending public key in file {}...'.format(remote_public_key_path))
            append(remote_public_key_path, public_key)
        debug('Checking if private key is in file {}...'.format(remote_private_key_path))
        if not contains(remote_private_key_path, private_key):
            debug('Appending private key in file {}...'.format(remote_private_key_path))
            append(remote_private_key_path, private_key)
        run('chmod 644 {}'.format(remote_public_key_path))
        run('chmod 600 {}'.format(remote_private_key_path))
        debug('Checking if bitbucket.org is in file {}...'.format(remote_private_known_hosts_path))
        if not contains(remote_private_known_hosts_path, 'bitbucket.org'):
            debug('Appending bitbucket.org in file {}...'.format(remote_private_known_hosts_path))
            run('ssh-keyscan bitbucket.org >> {}'.format(remote_private_known_hosts_path))


def check_repository():
    debug('Checking if the project was uploaded to bitbucket...')
    url = 'https://bitbucket.org/api/2.0/repositories'
    command = 'curl -X GET -u "{}:{}" {}/{}/{}'.format(bitbucket_username, bitbucket_password, url, bitbucket_username, project_name)
    data = json.loads(local(command, capture=True))
    if 'error' in data:
        debug('Uploading project to bitbucket...')
        command = 'curl -X POST -u "{}:{}" {}/{}/{}  -d \'{{"name":"{}"}}\''.format(bitbucket_username, bitbucket_password, url, bitbucket_username, project_name, project_name)
        data = json.loads(local(command, capture=True))
    repository_url = data['links']['clone'][1]['href']
    debug('Project uploaded to bitbucket! URL: {}'.format(repository_url))
    return repository_url


def setup_local_repository():
    debug('Checking if local project is a git project...')
    if not path.exists(path.join(project_dir, '.git')):
        with cd(project_dir):
            debug('Making local project a git project...')
            repository_url = check_repository()
            local('git init')
            local('git remote add origin "{}"'.format(repository_url))
            local('echo "..." > README.md')
            local('echo "{}" > .gitignore'.format(GIT_INGORE_FILE_CONTENT))


def setup_remote_repository():
    debug('Checking if the project was cloned in remote server...')
    if not exists(remote_project_dir):
        with cd('/var/opt'):
            debug('Cloning project in remote server...')
            repository_url = check_repository()
            run('git clone {} {}'.format(repository_url, project_name))
            run('chown -R www-data.www-data {}'.format(project_name))
    debug('Updating project in remote server...')
    with cd(remote_project_dir):
        run('git pull origin master')


def push_local_changes():
    debug('Checking if project has local changes...')
    now = datetime.datetime.now().strftime("%Y%m%d %H:%M:%S")
    with cd(project_dir):
        if 'nothing to commit' not in local('git status', capture=True):
            debug('Comminting local changes...')
            files = []
            for file_name in local('ls', capture=True).split():
                if file_name not in GIT_INGORE_FILE_CONTENT or file_name == 'fabfile.py':
                    files.append(file_name)
            files.append('.gitignore')
            for pattern in NGINEX_FILE_CONTENT.split():
                if pattern in files: files.remove(pattern)
            local('git add {}'.format(' '.join(files)))
            local("git commit -m '{}'".format(now))
        debug('Uploading local changes...')
        local('git push origin master')


def setup_remote_env():
    debug('Checking if the virtualenv dir was created in remote server...')
    virtual_env_dir = '/var/opt/.virtualenvs'
    if not exists(virtual_env_dir):
        debug('Creating dir {}'.format(virtual_env_dir))
        run('mkdir -p {}'.format(virtual_env_dir))
    project_env_dir = path.join(virtual_env_dir, project_name)
    djangoplus_dir = path.join(project_env_dir, 'djangoplus')
    src_djangoplus_dir = path.join(djangoplus_dir, 'djangoplus')
    target_djangoplus_dir = path.join(project_env_dir, 'lib/python3.4/site-packages/djangoplus')
    src_pessoas_dir = path.join(djangoplus_dir, 'djangoplus-pessoas/pessoas')
    target_pessoas_dir = path.join(project_env_dir, 'lib/python3.4/site-packages/pessoas')
    src_enderecos_dir = path.join(djangoplus_dir, 'djangoplus-enderecos/enderecos')
    target_enderecos_dir = path.join(project_env_dir, 'lib/python3.4/site-packages/enderecos')
    if exists(target_djangoplus_dir) and not exists(src_djangoplus_dir):
        run('mv {} {}'.format(target_djangoplus_dir, djangoplus_dir))
    debug('Checking if virtualenv for the project was created...')
    if not exists(project_env_dir):
        with shell_env(WORKON_HOME=virtual_env_dir):
            debug('Creating virtual env {}'.format(project_name))
            run('source /usr/local/bin/virtualenvwrapper.sh && mkvirtualenv --python=/usr/bin/python3 {}'.format(project_name))
    if not exists(djangoplus_dir):
            run('git clone -b master {} {}'.format(djangoplus_repository_url, djangoplus_dir))
    with cd(djangoplus_dir):
        debug('Updating djangoplus project virtual env...')
        run('git pull origin master')
        run("find . -name '*.pyc' -delete")
        if not exists(target_djangoplus_dir):
            run('ln -s {} {}'.format(src_djangoplus_dir, target_djangoplus_dir))
        if not exists(target_pessoas_dir):
            run('ln -s {} {}'.format(src_pessoas_dir, target_pessoas_dir))
        if not exists(target_enderecos_dir):
            run('ln -s {} {}'.format(src_enderecos_dir, target_enderecos_dir))


def setup_remote_project():
    with cd(remote_project_dir):
        debug('Checking project requirements..')
        if exists('requirements.txt'):
            virtual_env_dir = '/var/opt/.virtualenvs'
            with shell_env(WORKON_HOME=virtual_env_dir):
                if project_name in ('petshop', 'loja', 'biblioteca'):
                    debug('Installing/Updating djangoplus requirements...')
                    run('source /usr/local/bin/virtualenvwrapper.sh && workon {} && pip install --upgrade pip'.format(project_name))
                    run('source /usr/local/bin/virtualenvwrapper.sh && workon {} && pip install -U -r /var/opt/.virtualenvs/{}/lib/python3.4/site-packages/djangoplus/requirements.txt'.format(project_name, project_name))
                    run('source /usr/local/bin/virtualenvwrapper.sh && workon {} && pip install http://djangoplus.net/72cde92422b53857899fffb82d92508c8d1492b3.tar.gz'.format(project_name))
                else:
                    debug('Installing/Updating project requirements...')
                    run('source /usr/local/bin/virtualenvwrapper.sh && workon {} && pip install --upgrade pip'.format(project_name))
                    run('source /usr/local/bin/virtualenvwrapper.sh && workon {} && pip install -U -r requirements.txt'.format(project_name))
        debug('Checking if necessary dirs (logs, media and static) were created...')
        run('mkdir -p logs')
        run('mkdir -p static')
        run('mkdir -p media')
        debug('Granting access to www-data...')
        run('chown -R www-data.www-data .')


def setup_nginx_file():
    domain_name = '{}.{}'.format(project_name, hostname)
    file_path = '/etc/nginx/sites-enabled/{} '.format(project_name)
    debug('Checking nginx file {}...'.format(file_path))
    if not exists(file_path):
        debug('Creating nginx file {}...'.format(file_path))
        port = available_port()
        data = dict(project_name=project_name, domain_name=domain_name, port=port)
        text = NGINEX_FILE_CONTENT % data
        append(file_path, text)
        debug('Restarting nginx...')
        run('/etc/init.d/nginx restart')


def setup_supervisor_file():
    file_path = '/etc/supervisor/conf.d/{}.conf '.format(project_name)
    debug('Checking supervisor file {}...'.format(file_path))
    if not exists(file_path):
        debug('Creating supervisor file {}...'.format(file_path))
        data = dict(project_name=project_name)
        text = SUPERVISOR_FILE_CONTENT % data
        append(file_path, text)
        debug('Reloading supervisorctl...')
        run('supervisorctl reload')


def setup_gunicorn_file():
    file_path = '/var/opt/{}/gunicorn_start.sh '.format(project_name)
    debug('Checking gunicorn file {}...'.format(file_path))
    if not exists(file_path):
        debug('Creating gunicorn file {}'.format(file_path))
        port = available_port()
        data = dict(project_name=project_name, port=port)
        text = GUNICORN_FILE_CONTENT % data
        append(file_path, text)
        run('chmod a+x {}'.format(file_path))


def setup_remote_webserver():
    setup_nginx_file()
    setup_supervisor_file()
    setup_gunicorn_file()


def reload_remote_application():
    debug('Updating project in remote server...')
    with cd(remote_project_dir):
        virtual_env_dir = '/var/opt/.virtualenvs'
        with shell_env(WORKON_HOME=virtual_env_dir):
            run('source /usr/local/bin/virtualenvwrapper.sh && workon {} && python manage.py sync'.format(project_name))
            run('chown -R www-data.www-data .')
            run('chmod a+w *.db')
            run('ls -l')
            debug('Restarting supervisorctl...')
            run('supervisorctl restart {}'.format(project_name))


def delete_remote_project():
    debug('Deleting remove project...')
    run('rm -r {}'.format(remote_project_dir))


def delete_remote_env():
    debug('Deleting remote env...')
    run('rmvirtualenv {}'.format(project_name))


def delete_repository():
    debug('Deleting project from bitbucket repository...')
    url = 'https://bitbucket.org/api/2.0/repositories'
    command = '''curl -X DELETE -u "{}:{}" {}/{}/{}'''.format(bitbucket_username, bitbucket_password, url, bitbucket_username, project_name)
    local(command)


def delete_local_repository():
    debug('Deleting local repository...')
    with cd(project_dir):
        local('rm -rf .git')


def delete_nginx_file():
    debug('Deleting nginx file...')
    file_path = '/etc/nginx/sites-enabled/{} '.format(project_name)
    run('rm {}'.format(file_path))


def delete_supervisor_file():
    debug('Deleting supervisor file..')
    file_path = '/etc/supervisor/conf.d/{}.conf '.format(project_name)
    run('rm {}'.format(file_path))


def reload_remote_webserver():
    debug('Reloading supervisorctl...')
    run('supervisorctl reload')
    debug('Reloading nginx...')
    run('/etc/init.d/nginx restart')
    debug('Starting supervisor...')
    run('service supervisor start')


def configure_crontab():
    debug('Configuring crontab...')
    output = run("crontab -l")
    line = '0 * * * * /var/opt/.virtualenvs/{}/bin/python /var/opt/{}/manage.py backup >/tmp/cron.log 2>&1'.format(project_name, project_name)
    if line not in output:
        run('crontab -l | { cat; echo "{}"; } | crontab -'.format(line))


def execute_aptget():
    with cd('/'):
        run('apt-get update')
        run('apt-get -y install build-essential python3 python3-pip python3-dev git nginx supervisor libncurses5-dev')
        run('apt-get -y install vim')
        run('apt-get -y install libjpeg62-turbo-dev libopenjpeg-dev libfreetype6-dev libtiff5-dev liblcms2-dev libwebp-dev tk8.6-dev libjpeg-dev')
        run('apt-get -y install htop')

        if not contains('/etc/security/limits.conf', '65536'):
            # print LIMITS_FILE_CONTENT
            append('/etc/security/limits.conf', LIMITS_FILE_CONTENT)

        run('pip install virtualenv virtualenvwrapper')

        if not contains('/root/.bashrc', 'WORKON_HOME'):
            # print BASHRC_FILE_CONTENT
            append('/root/.bashrc', BASHRC_FILE_CONTENT)

        if not exists('/swap.img'):
            run('lsb_release -a')
            run('dd if=/dev/zero of=/swap.img bs=1024k count=2000')
            run('mkswap /swap.img')
            run('swapon /swap.img')
            run('echo "/swap.img    none    swap    sw    0    0" >> /etc/fstab')


def undeploy():
    delete_remote_project()
    delete_remote_env()
    delete_repository()
    delete_local_repository()
    delete_nginx_file()
    delete_supervisor_file()
    reload_remote_webserver()


def deploy():
    #with hide('running','warnings'):#, 'output'
    check_remote_keys()
    # execute_aptget()
    setup_local_repository()
    push_local_changes()
    setup_remote_env()
    setup_remote_repository()
    setup_remote_project()
    setup_remote_webserver()
    reload_remote_application()


def update():
    push_local_changes()
    setup_remote_env()
    setup_remote_repository()
    setup_remote_project()
    reload_remote_application()


def push():
    push_local_changes()
    setup_remote_repository()
    reload_remote_application()


def pwd():
    run('pwd')


env.hosts = ['{}:2046'.format(hostname)]
env.user = username
env.connection_attempts = 10
