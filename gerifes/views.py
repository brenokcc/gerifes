# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib import auth
from djangoplus.admin.models import User
from django.http.response import HttpResponseRedirect
from djangoplus.ui.components.navigation.breadcrumbs import httprr
from djangoplus.utils.http import ReportResponse

from gerifes.forms import ImportarServidorForm
from gerifes.models import *
from djangoplus.decorators.views import view, action, dashboard
from djangoplus.ui.components.utils import ModelReport, Chart
from djangoplus.ui.components.paginator import Paginator

# VIEWS


@view('Oauth', login_required=False)
def oauth(request):
    code = request.GET.get('code')
    url = settings.DEBUG and 'gerifes.ufrn.br' or 'gerifes.ufrn.br'
    secret = settings.UFRN_SECRET
    key = settings.UFRN_KEY
    response = requests.post('https://autenticacao.ufrn.br/authz-server/oauth/token?client_id=gerifes-id&client_secret={}&redirect_uri=http://{}/gerifes/oauth/&grant_type=authorization_code&code={}'.format(secret, url, code))
    token = json.loads(response.content.decode('utf-8'))['access_token']
    response = json.loads(requests.get('https://api.ufrn.br/usuario/v0.1/usuarios/info', headers={'Authorization': 'bearer {}'.format(token), 'x-api-key': key}).content.decode('utf-8'))
    cpf = response['cpf-cnpj']
    servidor = Servidor.objects.filter(login=cpf).first()
    if servidor is None:
        instituicao = InstituicaoEnsino.objects.get(pk=InstituicaoEnsino.UFRN)
        instituicao.importar_servidores(cpf)
    user = User.objects.get(username=cpf)
    auth.login(request, user)
    return HttpResponseRedirect('/admin/')


def oauth_logout(request):
    requests.post('https://sipac.ufrn.br/sipac/j_spring_cas_security_logout')
    return HttpResponseRedirect('/')


@view('Info', login_required=False)
def info(request, tipo):
    acervo = []
    instituicoes = InstituicaoEnsino.objects.all().order_by('pk')
    for choice in Arquivo.CATEGORIA_CHOICES:
        acervo.append((choice[0], Arquivo.objects.filter(categoria=choice[0])))
    videos = ['Acessando o Sistema', 'Cadastrando Usuários', 'Alterando Cadastro Geral', 'Cadastrando Macroporcessos/Pocessos', 'Cadastrando Objetivos Estratégicos/Operacionais', 'Cadastrando Eventos', 'Dashboard', 'Relatórios Gerenciais', 'Gerenciando os Riscos']
    return locals()


@action(Servidor, 'Importar Servidor', can_execute='Administrador', inline=True)
def importar_servidor(request):
    form = ImportarServidorForm(request)
    if form.is_valid():
        servidor = form.processar()
        return httprr(request, '..', '{} importado com sucesso.'.format(servidor))
    return locals()


# WIDGETS #


@dashboard(position='right', can_view=('Facilitador', 'Gestor de Risco', 'Servidor'))
def widget_imagem_processo(request):
    return locals()


@dashboard(position='center', can_view=('Facilitador', 'Gestor de Risco', 'Servidor'))
def widget_abrangencia(request):
    total = Processo.objects.all(request.user).count()
    servidor = Servidor.objects.get(login=request.user.username)
    eventos = EventoOperacional.objects.filter(gerenciador__servidor__instituicao=servidor.instituicao)
    qtd = eventos.order_by('subprocesso__processo').values_list('subprocesso__processo', flat=True).distinct().count()
    percentual_abrangencia = total and int(qtd*100/total) or 0
    eventos = Evento.objects.filter(gerenciador__servidor__instituicao=servidor.instituicao)
    qtd_inerente = eventos.filter(risco__in=('Alto', 'Muito Alto')).count()
    qtd_residual = eventos.filter(risco__in=('Alto', 'Muito Alto'), risco_residual__in=('Baixo', 'Moderado')).count()
    diferenca = qtd_inerente - qtd_residual
    percentual_eficacia = round(qtd_residual * 100.0 / qtd_inerente if qtd_inerente else 0)

    return locals()


@dashboard(position='center', can_view=('Facilitador', 'Gestor de Risco', 'Servidor'))
def widget_matriz(request):
    from django.conf import settings
    cores = settings.GRADIENT
    probabilidades = ['Muito Alto', 'Alto', 'Moderado', 'Baixo', 'Muito Baixo']
    impactos = ['Insignificante', 'Pequeno', 'Médio', 'Grande', 'Crítico']

    def contar(i, j):
        probabilidade = probabilidades[i]
        impacto = impactos[j]
        qtd = Evento.objects.all(request.user).filter(probabilidade=probabilidade, impacto_residual=impacto).exclude(risco__isnull=True).count()
        descricao = '{} evento(s) com valor de probabilidade "{}" e impacto "{}"'.format(qtd, probabilidade, impacto)
        return qtd, descricao

    matriz = [
        ['Muito Alto', (cores[1], contar(0, 0)), (cores[2], contar(0, 1)), (cores[3], contar(0, 2)), (cores[3], contar(0, 3)), (cores[3], contar(0, 4))],
        ['Alto', (cores[1], contar(1, 0)), (cores[2], contar(1, 1)), (cores[2], contar(1, 2)), (cores[3], contar(1, 3)), (cores[3], contar(1, 4))],
        ['Moderado', (cores[1], contar(2, 0)), (cores[1], contar(2, 1)), (cores[2], contar(2, 2)), (cores[2], contar(2, 3)), (cores[3], contar(2, 4))],
        ['Baixo', (cores[0], contar(3, 0)), (cores[1], contar(3, 1)), (cores[1], contar(3, 2)), (cores[2], contar(3, 3)), (cores[2], contar(3, 4))],
        ['Muito Baixo', (cores[0], contar(4, 0)), (cores[0], contar(4, 1)), (cores[1], contar(4, 2)), (cores[1], contar(4, 3)), (cores[1], contar(4, 4))],
    ]
    return locals()


# RELATÓRIOS #


def relatorio(request, description, model, list_display, list_filter=(), distinct=False):
    qs = model.objects.all(request.user).filter(**{'{}__isnull'.format(list_display[0]): False})
    paginator = ModelReport(request, description, qs, list_display, list_filter, distinct)
    if request.GET.get('pdf', False):
        paginator.title = ''
        return ReportResponse(description, request, [paginator], template='report1.html', landscape=False)
    return locals()


@view('Cadeia de valor', can_view=('Administrador', 'Facilitador', 'Gestor de Risco', 'Servidor'), menu='Relatórios::1 Objetivos Organizacionais::1 Cadeia de Valor', icon='fa-file-text-o')
def relatorio_1(request):
    return relatorio(request, 'Cadeia de Valor', Subprocesso, ('processo__macroprocesso', 'processo', 'descricao'), ('processo__macroprocesso', 'processo'))


@view('Cadeia de valor', can_view=('Administrador', 'Facilitador', 'Gestor de Risco', 'Servidor'), menu='Relatórios::1 Objetivos Organizacionais::1 Objetivos Estratégicos', icon='fa-file-text-o')
def relatorio_2(request):
    return relatorio(request, 'Objetivos Estratégicos', ObjetivoEstrategico, ('plano_gestao__pdi', 'plano_gestao', 'descricao'), ('plano_gestao__pdi', 'plano_gestao'))


@view('Objetivos Operacionais', can_view=('Administrador', 'Facilitador', 'Gestor de Risco', 'Servidor'), menu='Relatórios::3 Eventos::1 Objetivos Operacionais', icon='fa-file-text-o')
def relatorio_3(request):
    return relatorio(request, 'Objetivos Operacionais', EventoOperacional, ('subprocesso__processo__macroprocesso', 'subprocesso__processo', 'subprocesso', 'cadastrador', 'tipos_risco', 'descricao'), ('subprocesso__processo__macroprocesso', 'subprocesso__processo', 'subprocesso', 'cadastrador__unidade'))


@view('Objetivos Estratégicos', can_view=('Administrador', 'Facilitador', 'Gestor de Risco', 'Servidor'), menu='Relatórios::3 Eventos::2 Objetivos Estratégicos', icon='fa-file-text-o')
def relatorio_4(request):
    return relatorio(request, 'Objetivos Estratégicos', EventoEstrategico, ('objetivo_estrategico__plano_gestao__pdi', 'objetivo_estrategico__plano_gestao', 'objetivo_estrategico', 'cadastrador', 'tipos_risco', 'descricao'), ('objetivo_estrategico__plano_gestao__pdi', 'objetivo_estrategico__plano_gestao', 'objetivo_estrategico', 'cadastrador__unidade'))


@view('Eventos por Resposta ao Risco', can_view=('Administrador', 'Facilitador', 'Gestor de Risco', 'Servidor'), menu='Relatórios::4 Tipos de Risco::1 Objetivos Operacionais', icon='fa-file-text-o')
def relatorio_5(request):
    return relatorio(request, 'Eventos por Resposta ao Risco - Objetivos Operacionais', EventoOperacional, ('subprocesso__processo__macroprocesso', 'subprocesso__processo', 'subprocesso', 'cadastrador', 'tipos_risco', 'descricao', 'get_dados_risco_inerente'), ('subprocesso', 'resposta_risco', 'tipos_risco', 'risco'))


@view('Eventos por Resposta ao Risco', can_view=('Administrador', 'Facilitador', 'Gestor de Risco', 'Servidor'), menu='Relatórios::4 Tipos de Risco::2 Objetivos Estratégicos', icon='fa-file-text-o')
def relatorio_6(request):
    return relatorio(request, 'Eventos por Resposta ao Risco - Objetivos Estratégicos', EventoEstrategico, ('objetivo_estrategico__plano_gestao__pdi', 'objetivo_estrategico__plano_gestao', 'objetivo_estrategico', 'cadastrador', 'tipos_risco', 'descricao', 'get_dados_risco_inerente'), ('objetivo_estrategico', 'resposta_risco', 'tipos_risco', 'risco'))


@view('Risco Inerente x Risco Residual', can_view=('Administrador', 'Facilitador', 'Gestor de Risco', 'Servidor'), menu='Relatórios::6 Risco Inerente x Risco Residual::1 Objetivos Operacionais', icon='fa-file-text-o')
def relatorio_7(request):
    return relatorio(request, 'Risco Inerente x Risco Residual - Objetivos Operacionais', EventoOperacional, ('subprocesso__processo__macroprocesso', 'subprocesso__processo', 'subprocesso', 'cadastrador', 'descricao', 'get_dados_risco_inerente', 'resposta_risco', 'get_dados_risco_residual'), ('subprocesso', 'cadastrador__unidade', 'cadastrador', 'resposta_risco'))


@view('Risco Inerente x Risco Residual', can_view=('Administrador', 'Facilitador', 'Gestor de Risco', 'Servidor'), menu='Relatórios::6 Risco Inerente x Risco Residual::2 Objetivos Estratégicos', icon='fa-file-text-o')
def relatorio_8(request):
    return relatorio(request, 'Risco Inerente x Risco Residual - Objetivos Estratégicos', EventoEstrategico, ('objetivo_estrategico__plano_gestao__pdi', 'objetivo_estrategico__plano_gestao', 'objetivo_estrategico', 'cadastrador', 'descricao', 'get_dados_risco_inerente', 'resposta_risco', 'get_dados_risco_residual'), ('objetivo_estrategico', 'cadastrador__unidade', 'cadastrador', 'resposta_risco'))


@view('Atividades Preventivas', can_view=('Administrador', 'Facilitador', 'Gestor de Risco', 'Servidor'), menu='Relatórios::4.1 Atividades Preventivas::1 Objetivos Operacionais', icon='fa-file-text-o')
def relatorio_9(request):
    return relatorio(request, 'Atividades Preventivas - Objetivos Operacionais', PlanoAcao, ('evento__eventooperacional__subprocesso__processo__macroprocesso', 'evento__eventooperacional__subprocesso__processo', 'evento__eventooperacional__subprocesso', 'evento__cadastrador', 'evento__tipos_risco', 'evento__descricao', 'descricao', 'data_prevista_conclusao', 'status'), ('evento__eventooperacional__subprocesso__processo__macroprocesso', 'status', 'evento__cadastrador'))


@view('Atividades Preventivas', can_view=('Administrador', 'Facilitador', 'Gestor de Risco', 'Servidor'), menu='Relatórios::4.1 Atividades Preventivas::2 Objetivos Estratégicos', icon='fa-file-text-o')
def relatorio_10(request):
    return relatorio(request, 'Atividades Preventivas - Objetivos Estratégicos', PlanoAcao, ('evento__eventoestrategico__objetivo_estrategico__plano_gestao__pdi', 'evento__eventoestrategico__objetivo_estrategico__plano_gestao', 'evento__eventoestrategico__objetivo_estrategico', 'evento__cadastrador', 'evento__tipos_risco', 'evento__descricao', 'descricao', 'data_prevista_conclusao', 'status'), ('evento__eventoestrategico__objetivo_estrategico__plano_gestao__pdi', 'status', 'evento__cadastrador'))


@view('Planos de Contingência', can_view=('Administrador', 'Facilitador', 'Gestor de Risco', 'Servidor'), menu='Relatórios::4.2 Planos de Contigência::1 Objetivos Operacionais', icon='fa-file-text-o')
def relatorio_11(request):
    return relatorio(request, 'Planos de Contingência - Objetivos Operacionais', PlanoContigencia, ('evento__eventooperacional__subprocesso__processo__macroprocesso', 'evento__eventooperacional__subprocesso__processo', 'evento__eventooperacional__subprocesso', 'evento__cadastrador', 'evento__tipos_risco', 'evento__descricao', 'descricao'), ('evento__eventooperacional__subprocesso__processo__macroprocesso', 'evento__cadastrador'))


@view('Planos de Contingência', can_view=('Administrador', 'Facilitador', 'Gestor de Risco', 'Servidor'), menu='Relatórios::4.2 Planos de Contigência::2 Objetivos Estratégicos', icon='fa-file-text-o')
def relatorio_12(request):
    return relatorio(request, 'Planos de Contingência - Objetivos Estratégicos', PlanoContigencia, ('evento__eventoestrategico__objetivo_estrategico__plano_gestao__pdi', 'evento__eventoestrategico__objetivo_estrategico__plano_gestao', 'evento__eventoestrategico__objetivo_estrategico', 'evento__cadastrador', 'evento__tipos_risco', 'evento__descricao', 'descricao'), ('evento__eventoestrategico__objetivo_estrategico__plano_gestao__pdi', 'evento__cadastrador'))


@action(EventoOperacional, 'Adicionar Atividades de Controle', subset='planos_nao_definidos', condition='atividades_pendentes', can_execute=('Administrador', 'Facilitador'))
def adicionar_atividades_controle_operacional(request, pk):
    return HttpResponseRedirect('/view/gerifes/eventooperacional/{}/atividades-preventivas/'.format(pk))


@action(EventoEstrategico, 'Adicionar Atividades de Controle', subset='planos_nao_definidos', condition='atividades_pendentes', can_execute=('Administrador', 'Facilitador'))
def adicionar_atividades_controle_estrategico(request, pk):
    return HttpResponseRedirect('/view/gerifes/eventoestrategico/{}/atividades-preventivas/'.format(pk))


@view('Painel', login_required=False)
def painel(request, pk, tipo='graficos'):
    instituicao = InstituicaoEnsino.objects.get(pk=pk)
    ids_unidades = [int(x) for x in request.GET.getlist('unidade')]
    ids_macroprocessos = [int(x) for x in request.GET.getlist('macroprocessos')]
    eventos = EventoOperacional.objects.filter(subprocesso__processo__macroprocesso__instituicao=pk)
    macroprocessos = Macroprocesso.objects.filter(id__in=eventos.values_list('subprocesso__processo__macroprocesso').distinct())
    unidades = UnidadeAdministrativa.objects.filter(id__in=eventos.values_list('cadastrador__unidade').distinct())
    if ids_unidades:
        eventos = eventos.filter(cadastrador__unidade__in=ids_unidades)
    if ids_macroprocessos:
        eventos = eventos.filter(subprocesso__processo__macroprocesso__in=ids_macroprocessos)
    processos = Processo.objects.filter(id__in=eventos.values_list('subprocesso__processo').distinct())
    subprocessos = Processo.objects.filter(id__in=eventos.values_list('subprocesso').distinct())
    atividades_preventivas = PlanoAcao.objects.filter(evento__in=eventos.values_list('id', flat=True).distinct())
    totais = list()
    totais.append((
        ('Macroprocessos Associados', None, eventos.values_list('subprocesso__processo__macroprocesso').distinct().count(), 50),
        ('Processos de Trabalho Associados', None, processos.count(), 50),
    ))
    totais.append((
        ('Riscos Mapeados', None, eventos.count(), 33),
        ('Riscos Críticos', None, eventos.filter(critico=True).count(), 33),
        ('Atividades de Controle', None, atividades_preventivas.exclude(status=PlanoAcao.NAO_INCIADO).count(), 33),
        # ('Riscos Validados pelo Comitê', None, eventos.filter(validado=True).count())
    ))
    x = eventos.count('cadastrador__unidade')
    h = int(len(x.labels)*0.6*100)
    chart1 = Chart(request, x.labels, x.series, title='Riscos por Setor', type='horizontalbar')
    x = eventos.count('risco_residual')
    chart2 = Chart(request, x.labels, x.series, title='Eventos por Grau de Risco', type='pie')
    x = eventos.count('tipos_risco')
    chart4 = Chart(request, x.labels, x.series, title='Eventos por Tipo de Risco', type='horizontalbar')
    x = atividades_preventivas.count('status')
    chart5 = Chart(request, x.labels, x.series, title='Atividades Preventivas por Status', type='pie')
    x = eventos.count('resposta_risco')
    chart6 = Chart(request, x.labels, x.series, title='Eventos por Reposta ao Risco', type='donut')
    paginator = Paginator(request, eventos, 'Detalhamento', readonly=True)
    return locals()
