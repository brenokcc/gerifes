# Generated by Django 2.1.7 on 2021-07-11 19:34

from django.db import migrations
import djangoplus.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('gerifes', '0046_subprocesso_objetivo'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='eventoestrategico',
            options={'verbose_name': 'Evento Estratégico', 'verbose_name_plural': 'Riscos dos Objetivos Estratégicos'},
        ),
        migrations.AlterModelOptions(
            name='eventooperacional',
            options={'verbose_name': 'Evento Operacional', 'verbose_name_plural': 'Riscos dos Objetivos Operacionais'},
        ),
        migrations.AlterField(
            model_name='evento',
            name='nivel_confianca',
            field=djangoplus.db.models.fields.CharField(blank=True, choices=[['Inexistente', 'Inexistente'], ['Fraco', 'Fraco'], ['Mediano', 'Mediano'], ['Satisfatório', 'Satisfatório'], ['Forte', 'Forte']], help_text='\nInexistente: Controles inexistentes, mal desenhados ou mal implementados, isto é, não funcionais.\n\nFraco: Controles têm abordagens ad hoc, tendem a ser aplicados caso a caso, a responsabilidade é individual, havendo elevado grau de confiança no conhecimento das pessoas.\n\nMediano: Controles implementados mitigam alguns aspectos do risco, mas não contemplam todos os aspectos relevantes do risco devido a deficiências no desenho ou nas ferramentas utilizadas.\n\nSatisfatório: Controles implementados e sustentados por ferramentas adequadas e, embora passíveis de aperfeiçoamento, mitigam o risco satisfatoriamente.\n\nForte: Controles implementados podem ser considerados a "melhor prática", mitigando todos os aspectos relevantes do risco.\n\n', max_length=255, null=True, verbose_name='Nível de Confiança'),
        ),
    ]
