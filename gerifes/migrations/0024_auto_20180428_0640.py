# Generated by Django 2.0.3 on 2018-04-28 06:40

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gerifes', '0023_auto_20180422_1452'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='administradorsistemico',
            name='servidor',
        ),
        migrations.DeleteModel(
            name='AdministradorSistemico',
        ),
    ]
