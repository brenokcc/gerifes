# Generated by Django 2.1.7 on 2019-03-17 16:13

from django.db import migrations
import djangoplus.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('gerifes', '0034_macroprocesso_unidades'),
    ]

    operations = [
        migrations.AddField(
            model_name='evento',
            name='validado',
            field=djangoplus.db.models.fields.NullBooleanField(verbose_name='Validado'),
        ),
    ]
