# Generated by Django 2.0.3 on 2018-04-10 12:24

from django.db import migrations
import djangoplus.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('gerifes', '0015_auto_20180407_1546'),
    ]

    operations = [
        migrations.AddField(
            model_name='servidor',
            name='login',
            field=djangoplus.db.models.fields.CharField(max_length=255, null=True, verbose_name='Login'),
        ),
    ]
