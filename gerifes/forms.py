# -*- coding: utf-8 -*-

from djangoplus.ui.components import forms
from djangoplus.decorators import action

from gerifes.models import InstituicaoEnsino


class ImportarServidorForm(forms.Form):

    class Meta:
        title = 'Importar Servidor'

    def __init__(self, *args, **kwargs):
        super(ImportarServidorForm, self).__init__(*args, **kwargs)
        self.instituicoes = InstituicaoEnsino.objects.all(self.request.user).order_by('pk')
        self.fields['cpf'] = forms.CpfField(label='CPF', required=False)
        self.fields['siape'] = forms.CharField(label='SIAPE', required=False)

    def processar(self):
        instituicao = self.instituicoes.first()
        cpf = self.cleaned_data['cpf'].replace('.', '').replace('-', '')
        siape = self.cleaned_data['siape']
        return instituicao.importar_servidores(cpf=cpf, siape=siape)