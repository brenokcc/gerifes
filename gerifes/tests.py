# -*- coding: utf-8 -*-

from django.conf import settings
from djangoplus.test import TestCase
from djangoplus.admin.models import User
from djangoplus.test.decorators import testcase


class AppTestCase(TestCase):
    def test(self):
        User.objects.create_superuser('admin', None, settings.DEFAULT_PASSWORD)
        self.execute_flow()

    @testcase('', username='admin')
    def cadastrar_origemevento(self):
        self.click_menu('Cadastro Geral', 'Origens dos Eventos')
        self.click_button('Cadastrar')
        self.enter('Origem do Evento', 'Interno')
        self.click_button('Salvar')
        self.click_icon('Principal')

    @testcase('', username='admin')
    def cadastrar_respostarisco(self):
        self.click_menu('Cadastro Geral', 'Respostas aos Riscos')
        self.click_button('Cadastrar')
        self.enter('Resposta ao Risco', 'Mitigar')
        self.click_button('Salvar')
        self.click_icon('Principal')

    @testcase('', username='admin')
    def cadastrar_classificacaomacroprocesso_referencia(self):
        self.click_menu('Cadastro Geral', 'Classificação de Macroprocessos')
        self.click_button('Cadastrar')
        self.enter('Classificação de Macroprocesso', 'De Apoio')
        self.click_button('Salvar')
        self.click_icon('Principal')

    @testcase('', username='admin')
    def cadastrar_tiporisco_referencia(self):
        self.click_menu('Cadastro Geral', 'Tipos de Risco Padrão')
        self.click_button('Cadastrar')
        self.choose('Origem do Evento', 'Interno')
        self.enter('Tipo de Risco', 'Pessoal')
        self.click_button('Salvar')
        self.click_icon('Principal')

    @testcase('', username='admin')
    def cadastrar_instituicao(self):
        self.click_menu('Instituições')
        self.click_button('Cadastrar')
        self.enter('Nome', 'UFRN')
        self.click_button('Salvar')

    @testcase('', username='admin')
    def importar_servidores(self):
        pass

    @testcase('', username='admin')
    def cadastrar_servidor_administrador(self):
        self.click_menu('Usuários', 'Servidores')
        self.click_button('Cadastrar')
        self.enter('Login', '1799479')
        self.enter('Lotação', 'COSINF')
        self.enter('Matrícula', '1799479')
        self.enter('Nome', 'Breno Silva')
        self.enter('E-mail', 'breno@gerifes.net')
        self.click_button('Salvar')

    @testcase('', username='admin')
    def cadastrar_administrador(self):
        self.click_menu('Usuários', 'Administradores do Sistema')
        self.click_button('Cadastrar')
        self.choose('Servidor', 'Breno Silva')
        self.click_button('Salvar')
        self.click_icon('Principal')

    @testcase('', username='1799479')
    def cadastrar_unidade(self):
        self.click_menu('Cadastro Geral', 'Unidades Gestoras de Risco')
        self.click_button('Cadastrar')
        self.enter('Descrição', 'PROEN')
        self.click_button('Salvar')
        self.click_icon('Principal')

    @testcase('', username='1799479')
    def cadastrar_macroprocesso(self):
        self.click_menu('Cadeia de Valor', 'Macroprocessos')
        self.click_button('Cadastrar')
        self.enter('Macroprocesso', 'Ensino')
        self.choose('Classificação', 'De Apoio')
        self.enter('Objetivo', 'Otimizar a gestão')
        self.choose('Unidade(s) Associada(s)', 'PROEN')
        self.click_button('Salvar')
        self.click_icon('Principal')

    @testcase('', username='1799479')
    def cadastrar_metaglobal(self):
        self.click_menu('Objetivos Organizacionais', 'Objetivos Estratégicos')
        self.click_button('Cadastrar')
        self.enter('Objetivo Estratégico', 'Otimização')
        self.click_button('Salvar')
        self.click_icon('Principal')

    @testcase('', username='1799479')
    def cadastrar_processo(self):
        self.click_menu('Cadeia de Valor', 'Processos')
        self.click_button('Cadastrar')
        self.choose('Macroprocesso', 'Ensino')
        self.enter('Processo', 'Otimizar o ensino')
        self.click_button('Salvar')
        self.click_icon('Principal')

    @testcase('', username='1799479')
    def cadastrar_servidor_facilitador(self):
        self.click_menu('Usuários', 'Servidores')
        self.click_button('Cadastrar')
        self.enter('Lotação', 'PROEN')
        self.enter('Login', '1111111')
        self.enter('Matrícula', '1111111')
        self.enter('Nome', 'Maria Paula')
        self.enter('E-mail', 'maria.paula@gerifes.net')
        self.click_button('Salvar')

    @testcase('', username='1799479')
    def cadastrar_facilitador(self):
        self.click_icon('Principal')
        self.click_menu('Usuários', 'Facilitadores')
        self.click_button('Cadastrar')
        self.choose('Servidor', 'Maria Paula')
        # self.choose('Unidade', 'PROEN')
        self.click_button('Salvar')

    @testcase('', username='1111111')
    def cadastrar_servidor_gestorrisco(self):
        self.click_menu('Usuários', 'Servidores')
        self.click_button('Cadastrar')
        self.enter('Login', '2222222')
        self.enter('Matrícula', '2222222')
        self.enter('Nome', 'Juca da Silva')
        self.enter('E-mail', 'juca.silva@gerifes.net')
        self.enter('Lotação', 'DIPED')
        self.click_button('Salvar')

    @testcase('', username='1111111')
    def cadastrar_gestorrisco(self):
        self.click_menu('Usuários', 'Gestores de Riscos')
        self.click_button('Cadastrar')
        self.choose('Servidor', 'Juca da Silva')
        # self.choose('Unidade', 'PROEN')
        self.click_button('Salvar')
        self.click_icon('Principal')

    @testcase('', username='1111111')
    def cadastrar_objetivooperacional(self):
        self.click_menu('Objetivos Organizacionais', 'Objetivos Operacionais')
        self.click_button('Cadastrar')
        self.choose('Processo', 'Otimizar o ensino')
        self.choose('Objetivos Estratégicos', 'Otimização')
        self.enter('Objetivo Operacional', 'Capacitar Professores')
        self.click_button('Salvar')
        self.click_icon('Principal')

    @testcase('', username='1111111')
    def cadastrar_evento(self):
        self.click_link('Eventos')
        self.click_button('Cadastrar')
        self.choose('Gestor de Risco', 'Juca da Silva')
        self.choose('Tipo de Risco', 'Pessoal')
        self.choose('Objetivo Operacional', 'Capacitar Professores')
        self.enter('Evento', 'Falta de recursos')
        self.click_button('Salvar')
        self.click_icon('Principal')

    @testcase('', username='1111111')
    def classificar_risco_inerente_em_evento(self):
        self.click_link('Eventos')
        self.click_link('Risco Inerente Não-Classificado')
        self.click_button('Classificar Risco Inerente')
        self.look_at_popup_window()
        self.enter('Causas', 'Contigenciamento')
        self.enter('Efeitos', 'Corte nas despesas')
        self.choose('Probabilidade', 'Moderado')
        self.choose('Impacto', 'Grande')
        self.click_button('Classificar Risco Inerente')
        self.click_icon('Principal')

    @testcase('', username='1111111')
    def alterar_classificacao_risco_inerente_em_evento(self):
        self.click_link('Eventos')
        self.click_icon('Visualizar')
        self.click_button('Ações')
        self.click_button('Alterar Classificação do Risco Inerente')
        self.look_at_popup_window()
        self.enter('Causas', 'Contigenciamento')
        self.enter('Efeitos', 'Corte nas despesas')
        self.choose('Probabilidade', 'Moderado')
        self.choose('Impacto', 'Grande')
        self.click_button('Alterar Classificação do Risco Inerente')
        self.click_icon('Principal')

    @testcase('', username='1111111')
    def resposta_ao_risco_em_evento(self):
        self.click_link('Eventos')
        self.click_link('Resposta ao Risco Inerente Pendente')
        self.look_at('Falta de recursos')
        self.click_button('Resposta ao Risco Inerente')
        self.look_at_popup_window()
        self.choose('Resposta ao Risco', 'Mitigar')
        self.click_button('Resposta ao Risco Inerente')
        self.click_icon('Principal')

    @testcase('', username='1111111')
    def cadastrar_planoacao(self):
        self.click_menu('Gerenciamento', 'Atividades Preventivas')
        self.click_button('Cadastrar')
        self.choose('Evento', 'Falta de recursos')
        self.enter('Atividade Preventiva', 'Otimizar os gastos')
        self.enter('Detalhamento', '...')
        self.enter('Previsão Conclusão', '01/01/2019')
        self.choose('Status', 'Iniciado')
        self.click_button('Salvar')
        self.click_icon('Principal')

    @testcase('', username='1111111')
    def cadastrar_planocontigencia(self):
        self.click_menu('Gerenciamento', 'Planos de Contingência')
        self.click_button('Cadastrar')
        self.choose('Evento', 'Falta de recursos')
        self.enter('Plano de Contingência', 'Otimizar os gastos')
        self.click_button('Salvar')
        self.click_icon('Principal')

    @testcase('', username='1111111')
    def finalizar_cadastro_atividades_controle_em_evento(self):
        self.click_link('Eventos')
        self.click_icon('Visualizar')
        self.click_button(u'Ações')
        self.click_button('Finalizar Atividades de Controle')
        self.click_icon('Principal')

    @testcase('', username='1111111')
    def classificar_risco_residual_em_evento(self):
        self.click_link('Eventos')
        self.click_tab('Risco Residual Não-Classificado')
        self.click_button('Classificar Risco Residual')
        self.look_at_popup_window()
        self.choose('Nível de Confiança', 'Mediano')
        self.click_button('Classificar Risco Residual')
        self.click_icon('Principal')

    @testcase('', username='1111111')
    def alterar_classificacao_risco_residual_em_evento(self):
        self.click_link('Eventos')
        self.click_icon('Visualizar')
        self.click_button('Ações')
        self.click_button('Alterar Classificação do Risco Residual')
        self.look_at_popup_window()
        self.choose('Nível de Confiança', 'Mediano')
        self.click_button('Alterar Classificação do Risco Residual')
        self.click_icon('Principal')

    def adicionar_fluxograma_em_subprocesso(self):
        self.click_link('Fluxogramas')
        self.click_icon('Visualizar')
        self.click_button('Adicionar Fluxograma')
        self.look_at_popup_window()
        self.enter('Arquivo', '')
        self.click_button('Salvar')
        self.click_icon('Principal')