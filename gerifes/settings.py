# -*- coding: utf-8 -*-

from djangoplus.conf.base_settings import *
from os.path import abspath, dirname, join, exists
from os import sep

BASE_DIR = abspath(dirname(dirname(__file__)))
PROJECT_NAME = __file__.split(sep)[-2]

STATIC_ROOT = join(BASE_DIR, 'static')
MEDIA_ROOT = join(BASE_DIR, 'media')

DROPBOX_TOKEN = '' # disponível em https://www.dropbox.com/developers/apps
DROPBOX_LOCALDIR = MEDIA_ROOT
DROPBOX_REMOTEDIR = '/'

DATABASES = {'default': {'ENGINE': 'django.db.backends.postgresql_psycopg2', 'NAME': 'gerifes', 'USER': 'postgres', 'PASSWORD': '', 'HOST': '127.0.0.1', 'PORT': '5432'}}

WSGI_APPLICATION = '%s.wsgi.application' % PROJECT_NAME

INSTALLED_APPS += (
    PROJECT_NAME, 'endless',
)

ROOT_URLCONF = '%s.urls' % PROJECT_NAME

if exists(join(BASE_DIR, 'logs')):
    DEBUG = False
    ALLOWED_HOSTS = ['*']
    SERVER_EMAIL = 'root@djangoplus.net'
    ADMINS = [('Admin', 'root@djangoplus.net')]
    HOST_NAME = 'gerifes.ufrn.br'
else:
    HOST_NAME = 'gerifes.ufrn.br'
    EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
    EMAIL_FILE_PATH = join(BASE_DIR, 'mail')

DROPBOX_TOKEN = ''
BACKUP_FILES = ['media', 'sqlite.db']

EXTRA_JS = ['/static/js/gerifes.js']
EXTRA_CSS = ['/static/css/gerifes.css']

GRADIENT = ['#5BA94B', '#2475BD', '#F8C636', '#EF8641', '#FFEC33', '#D8D8D8', '#5DA0FC']

UFRN_KEY = 'cb6z6asXOVVbdpBrbBjoNj48g8AWqgw0ycPBA8ox'
UFRN_SECRET = '69eDCG3F5rvrTfwq8eaxMmuZhqgWF22y'

SENDGRID_KEY = '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'
DEFAULT_FROM_EMAIL = 'nao-responder@em731.gerifes.net'

