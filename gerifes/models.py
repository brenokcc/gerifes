# -*- coding: utf-8 -*-
import datetime
from django.conf import settings
from django.core.exceptions import ValidationError
from djangoplus.admin.models import Organization, Unit, User
from djangoplus.db import models
from djangoplus.db.models import QueryStatistics
from djangoplus.decorators import meta, action, subset, role
import requests
import json


class RespostaRisco(models.Model):
    descricao = models.CharField(verbose_name='Resposta ao Risco', search=True, example='Mitigar')

    class Meta:
        verbose_name = 'Resposta ao Risco'
        verbose_name_plural = 'Respostas aos Riscos'
        menu = '1 Cadastro Geral::5 Respostas aos Riscos'
        can_admin = 'Administrador Sistêmico'
        can_list = 'Gestor de Risco', 'Facilitador', 'Servidor', 'Administrador'
        list_display = 'descricao',
        usecase = 2

    def __str__(self):
        return self.descricao


class ClassificacaoMacroprocessoReferencia(models.Model):
    descricao = models.CharField(verbose_name='Classificação de Macroprocesso', search=True, example='De Apoio')

    class Meta:
        verbose_name = 'Classificação de Macroprocesso'
        verbose_name_plural = 'Classificação de Macroprocessos'
        menu = '1 Cadastro Geral::1 Classificação de Macroprocessos', 'fa-th'
        can_admin = 'Administrador Sistêmico'
        usecase = 4

    def __str__(self):
        return self.descricao


class TipoRiscoReferencia(models.Model):
    descricao = models.CharField(verbose_name='Tipo de Risco', search=True, example='Pessoal')

    class Meta:
        verbose_name = 'Tipo de Risco'
        verbose_name_plural = 'Tipos de Riscos'
        menu = '1 Cadastro Geral::4 Tipos de Risco Padrão'
        can_admin = 'Administrador Sistêmico'
        usecase = 5

    def __str__(self):
        return self.descricao


class InstituicaoEnsino(Organization):

    UFRN = 1023

    nome = models.CharField(verbose_name='Nome', search=True, example='UFRN')
    logo = models.ImageField(verbose_name='Logo', upload_to='logos', null=True, blank=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('nome', 'logo')}),
        ('Unidades::Unidades', {'relations': ('unidadeadministrativa_set',)}),
        ('Servidores::Servidores', {'relations': ('servidor_set',), 'actions' : ('importar_servidores',)}),
    )

    class Meta:
        verbose_name = 'Instituição'
        verbose_name_plural = 'Instituições'
        menu = '3 Instituições', 'fa-building'
        can_admin = 'Administrador Sistêmico'
        list_shortcut = 'Administrador Sistêmico'
        can_view_by_organization = 'Administrador', 'Servidor'
        usecase = 6
        icon = 'fa-building'

    def __str__(self):
        return self.nome

    def save(self, *args, **kwargs):
        inicializar_cadastro = not self.pk
        super(InstituicaoEnsino, self).save(*args, **kwargs)
        if inicializar_cadastro:
            for classificacao in ClassificacaoMacroprocessoReferencia.objects.all():
                ClassificacaoMacroprocesso.objects.create(descricao=classificacao.descricao, instituicao=self)
            for tipo_risco in TipoRiscoReferencia.objects.all():
                TipoRisco.objects.create(descricao=tipo_risco.descricao, instituicao=self)

    # @action('Importar Servidor - SIG', can_execute='Administrador Sistêmico', category=None, usecase=7)
    def importar_servidores(self, cpf=None, siape=None):
        client_id = 'gerifes-id'
        client_secret = settings.UFRN_SECRET
        api_key = settings.UFRN_KEY
        url_base = 'https://api.ufrn.br/'
        url_token = '{}authz-server/oauth/token?client_id={}&client_secret={}&grant_type=client_credentials'.format(
            url_base, client_id, client_secret)
        requisicao_token = requests.post(url_token)
        resposta = json.loads(requisicao_token.content.decode('utf-8'))
        token = resposta['access_token']
        params = ''
        if cpf:
            params = '?cpf={}'.format(cpf)
        if siape:
            params = '?siape={}'.format(siape)
        url = '{}pessoa/v0.1/servidores{}'.format(url_base, params)
        headers = {'Authorization': 'bearer ' + token, 'x-api-key': api_key}
        resposta = json.loads(requests.get(url, headers=headers).content.decode('utf-8'))
        for registro in resposta:
            matricula = '{}'.format(registro['siape'])
            cpf = registro['cpf']
            nome = registro['nome']
            lotacao = registro['lotacao']
            servidor = Servidor.objects.filter(login=cpf).first()
            if servidor is None:
                servidor = Servidor()
            servidor.nome = nome
            servidor.lotacao = lotacao
            servidor.matricula = matricula
            servidor.instituicao = self
            servidor.login = cpf
            servidor.save()
            url = '{}pessoa/v0.1/pessoas/{}/'.format(url_base, cpf)
            resposta = json.loads(requests.get(url, headers=headers).content.decode('utf-8'))
            url = '{}usuario/v0.1/usuarios/?cpf-cnpj={}'.format(url_base, cpf)
            resposta = json.loads(requests.get(url, headers=headers).content.decode('utf-8'))
            url = '{}usuario/v0.1/usuarios/{}/'.format(url_base, resposta[0]['id-usuario'])
            resposta = json.loads(requests.get(url, headers=headers).content.decode('utf-8'))
            return servidor


@role('servidor__login', scope='servidor__instituicao')
class Administrador(models.Model):
    """
    O Administrador do Sistema tem acesso a todas as funcionalidades do sistema, inclusive ao cadastro dos macroprocessos e processos da cadeia de valor da IFES, dos objetivos estratégicos e operacionais e ao de usuários (Administrador do Sistema, Facilitador e Gestor de Riscos). Tem permissão também para alterar o “Cadastro Geral”.
    Considerando que este perfil tem acesso a todas as funcionalidades do sistema, é recomendável que ele seja atribuído ao menor número possível de usuários. Recomenda-se, também, que o cadastro dos eventos de risco e das demais etapas do processo de gestão de riscos sejam realizados pelos servidores que detenham o perfil de Facilitador.
    """
    servidor = models.ForeignKey('gerifes.Servidor', verbose_name='Servidor', filter=('instituicao',), search=('nome', 'matricula'))

    class Meta:
        verbose_name = 'Administrador'
        verbose_name_plural = 'Administradores'
        menu = '7 Usuários::Administradores do Sistema', 'fa-users'
        can_admin = 'Administrador Sistêmico'
        can_admin_by_organization = 'Administrador'
        list_lookups = 'servidor__instituicao',
        list_display = 'servidor', 'get_matricula'
        usecase = 8

    def __str__(self):
        return self.servidor.nome

    @meta('Matrícula')
    def get_matricula(self):
        return self.servidor.matricula


@role('login', email='email', scope='instituicao')
class Servidor(models.Model):
    """
    O servidor acessa o sistema para acompanhar a gestão de risco de sua instituição. Ele pode apenas visualiar os cadastros e gerar relatórios.
    """
    instituicao = models.ForeignKey(InstituicaoEnsino, verbose_name='Instituição', null=True, example='UFRN', filter=True)
    login = models.CharField(verbose_name='Login', null=True)
    matricula = models.CharField(verbose_name='Matrícula', search=True, example='1799479')
    nome = models.CharField(verbose_name='Nome', search=True, example='Breno Silva')
    lotacao = models.CharField(verbose_name='Lotação', search=True, example='Pro-reitoria de Ensino')
    email = models.EmailField(verbose_name='E-mail', null=True, blank=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('instituicao', ('matricula', 'nome'), ('email', 'lotacao'))}),
        ('Dados de Acesso', {'fields': ('login',)})
    )

    class Meta:
        verbose_name = 'Servidor'
        verbose_name_plural = 'Servidores'
        can_list_by_organization = 'Administrador', 'Facilitador'
        list_lookups = 'instituicao'
        select_display = 'matricula', 'nome', 'lotacao'
        can_admin = 'Administrador Sistêmico'
        can_admin_by_organization = 'Facilitador', 'Administrador'
        menu = '7 Usuários::Servidores'

    def __str__(self):
        return self.nome

    def save(self, *args, **kwargs):
        pk = self.pk
        super(Servidor, self).save(*args, **kwargs)
        if not pk:
            user = User.objects.get(username=self.login)
            if user.email:
                user.send_access_invitation_for_group('Servidor')


class UnidadeAdministrativa(Unit):
    instituicao = models.ForeignKey(InstituicaoEnsino, verbose_name='Instituição', example='UFRN')
    descricao = models.CharField('Descrição', example='PROEN')

    fieldsets = (
        ('Dados Gerais', {'fields': ('instituicao', 'descricao',)}),
        ('Gestores de Risco::Gestores de Risco', {'relations': ('get_gestores_risco',)}),
        ('Macroprocessos Relacionados::Macroprocessos', {'relations': ('get_macroprocessos_relacionados',)}),
    )

    class Meta:
        verbose_name = 'Unidade'
        verbose_name_plural = 'Unidades'
        can_admin_by_organization = 'Administrador'
        can_list_by_organization = 'Servidor', 'Gestor de Risco'
        menu = '1 Cadastro Geral::0 Unidades Gestoras de Risco', 'fa-th'
        usecase = 9

    def __str__(self):
        return self.descricao

    @meta('Gestores de Riso')
    def get_gestores_risco(self):
        return self.gestorrisco_set.all()

    @meta('Macroprocessos Relacionados')
    def get_macroprocessos_relacionados(self):
        pks = Macroprocesso.objects.filter(processo__subprocesso__evento__cadastrador__unidade=self.pk).values_list('pk', flat=True).distinct()
        return Macroprocesso.objects.filter(pk__in=pks)


class ClassificacaoMacroprocesso(models.Model):
    instituicao = models.ForeignKey(InstituicaoEnsino, verbose_name='Instituição', null=True, example='UFRN', blank=True)
    descricao = models.CharField(verbose_name='Classificação de Macroprocesso', search=True, example='De Apoio')

    class Meta:
        verbose_name = 'Classificação de Macroprocesso'
        verbose_name_plural = 'Classificação de Macroprocessos'
        menu = '1 Cadastro Geral::1 Classificação de Macroprocessos', 'fa-th'
        can_admin = 'Administrador Sistêmico'
        can_list_by_organization = 'Gestor de Risco', 'Facilitador', 'Servidor'
        can_admin_by_organization = 'Administrador'
        list_display = 'descricao',

    def __str__(self):
        return self.descricao


class TipoRisco(models.Model):
    instituicao = models.ForeignKey(InstituicaoEnsino, verbose_name='Instituição', null=True, example='UFRN', blank=True)
    descricao = models.CharField(verbose_name='Tipo de Risco', search=True, example='Pessoal')

    class Meta:
        verbose_name = 'Tipo de Risco'
        verbose_name_plural = 'Tipos de Riscos'
        menu = '1 Cadastro Geral::4 Tipos de Risco'
        can_admin = 'Administrador Sistêmico'
        can_list_by_organization = 'Gestor de Risco', 'Facilitador', 'Servidor', 'Administrador'
        can_admin_by_organization = 'Administrador'
        list_display = 'descricao',

    def __str__(self):
        return self.descricao


class Macroprocesso(models.Model):
    instituicao = models.ForeignKey(InstituicaoEnsino, verbose_name='Instituição', null=True, example='UFRN')
    descricao = models.CharField(verbose_name='Macroprocesso', search=True, example='Ensino')
    classificacao_macroprocesso = models.ForeignKey(ClassificacaoMacroprocesso, verbose_name='Classificação', null=True, example='De Apoio')
    objetivo = models.CharField(verbose_name='Objetivo', search=True, example='Otimizar a gestão')
    unidades = models.ManyToManyField(UnidadeAdministrativa, verbose_name='Unidade(s) Associada(s)', blank=False, filter=True)

    fieldsets = (('Dados Gerais', {'fields': ('instituicao', 'descricao', 'classificacao_macroprocesso', 'objetivo')}),
                 ('Processos', {'relations': ('processo_set',)}),
                 ('Unidade(s) Associada(s)', {'relations': ('unidades',)}),
                )

    class Meta:
        verbose_name = 'Macroprocesso'
        verbose_name_plural = 'Macroprocessos'
        menu = '4 Cadeia de Valor::Macroprocessos', 'fa-sitemap'
        can_admin_by_organization = 'Administrador'
        can_list_by_organization = 'Facilitador'
        can_list_by_organization = 'Gestor de Risco', 'Servidor'
        list_display = 'descricao', 'classificacao_macroprocesso', 'objetivo', 'unidades'
        usecase = 10

    def __str__(self):
        return self.descricao


@role('servidor__login', email='servidor__email', scope='servidor__instituicao')
class Facilitador(models.Model):
    """
    O perfil de Facilitador deverá ser atribuído a servidor que detenha conhecimentos avançados em gestão de riscos e de processo, com habilidades para identificar riscos a partir de fluxogramas e conduzir oficinas de gestão de riscos. Ele tem permissão para cadastrar gestores de riscos, objetivos operacionais, eventos de risco e as demais etapas do processo de gestão de riscos (classificação do risco inerente, resposta ao risco, definição de atividades de controle e classificação do risco residual).
    """
    servidor = models.ForeignKey(Servidor, verbose_name='Servidor', null=True, search=('nome', 'matricula'))

    fieldsets = (('Dados Gerais', {'relations': ('servidor',)}),)

    class Meta:
        verbose_name = 'Facilitador'
        verbose_name_plural = 'Facilitadores'
        menu = '7 Usuários::Facilitadores', 'fa-users'
        can_admin_by_organization = 'Administrador'
        list_lookups = 'servidor__instituicao'
        list_display = 'servidor', 'get_matricula'
        usecase = 12

    def __str__(self):
        return self.servidor and self.servidor.nome or 'A definir'


    @meta('Matrícula')
    def get_matricula(self):
        return self.servidor.matricula

@role('servidor__login', email='servidor__email', scope='unidade')
class GestorRisco(models.Model):
    """
    O Gestor de Riscos é o perfil atribuído aos gestores responsáveis pelos três níveis de gestão da organização (estratégico, tático e operacional) ou a qualquer outro servidor, que por algum motivo, venha a ser definido como o responsável pelo acompanhamento das atividades de controle de algum evento de risco.
    """
    servidor = models.ForeignKey(Servidor, verbose_name='Servidor', null=True, search=('nome', 'matricula'))
    unidade = models.ForeignKey(UnidadeAdministrativa, verbose_name='Unidade Gestora de Risco', null=True, form_filter='servidor__instituicao', example='PROEN', filter=True)

    fieldsets = (('Dados Gerais', {'relations': ('servidor', 'unidade')}),)

    class Meta:
        verbose_name = 'Gestor de Risco'
        verbose_name_plural = 'Gestores de Risco'
        menu = '7 Usuários::Gestores de Riscos', 'fa-users'
        can_admin_by_organization = 'Facilitador', 'Administrador'
        can_view_by_organization = 'Gestor de Risco', 'Servidor'
        list_lookups = 'servidor__instituicao'
        list_display = 'servidor', 'get_matricula', 'unidade'
        usecase = 13

    def __str__(self):
        return self.servidor and self.servidor.nome or 'A definir'

    @meta('Matrícula')
    def get_matricula(self):
        return self.servidor.matricula


class Processo(models.Model):
    macroprocesso = models.ForeignKey(Macroprocesso, verbose_name='Macroprocesso', search='unidades__descricao', filter=('id', 'unidades'), example='Ensino')
    descricao = models.CharField(verbose_name='Processo', search=True, example='Otimizar o ensino')

    fieldsets = (('Dados Gerais', {'fields': ('macroprocesso', 'descricao')}),)

    class Meta:
        verbose_name = 'Processo'
        verbose_name_plural = 'Processos'
        menu = '4 Cadeia de Valor::Processos', 'fa-sitemap'
        can_admin_by_organization = 'Administrador'
        can_list_by_organization = 'Facilitador'
        can_list_by_organization = 'Gestor de Risco', 'Servidor'
        list_lookups = 'macroprocesso__instituicao',
        select_display = 'descricao', 'macroprocesso__unidades'
        list_display = 'macroprocesso', 'descricao', 'get_unidades'
        usecase = 14

    def __str__(self):
        return self.descricao

    @meta('Unidades')
    def get_unidades(self):
        return UnidadeAdministrativa.objects.filter(macroprocesso=self.macroprocesso)


class PDI(models.Model):
    instituicao = models.ForeignKey(InstituicaoEnsino, verbose_name='Instituição', null=True, example='UFRN')
    descricao = models.CharField(verbose_name='PDI', search=True, example='2020')

    fieldsets = (
        ('Dados Gerais', {'fields': ('descricao',)}),
    )

    class Meta:
        verbose_name = 'PDI'
        verbose_name_plural = 'PDI'
        menu = '1 Cadastro Geral::6 PDI'
        list_display = 'descricao',
        can_list_by_organization = 'Servidor', 'Gestor de Risco'
        can_admin_by_organization = 'Facilitador', 'Administrador'
        can_view_by_organization = 'Gestor de Risco'
        list_lookups = 'instituicao',

    def __str__(self):
        return self.descricao


class PlanoGestao(models.Model):
    pdi = models.ForeignKey(PDI, verbose_name='PDI', null=True, example='UFRN', filter=True)
    descricao = models.CharField(verbose_name='Plano Estratégico', search=True, example='2020')

    fieldsets = (
        ('Dados Gerais', {'fields': ('pdi', 'descricao',)}),
    )

    class Meta:
        verbose_name = 'Plano Estratégico'
        verbose_name_plural = 'Planos de Gestão'
        menu = '1 Cadastro Geral::7 Planos de Gestão'
        list_display = 'pdi', 'descricao'
        can_list_by_organization = 'Servidor', 'Gestor de Risco'
        can_admin_by_organization = 'Facilitador', 'Administrador'
        can_view_by_organization = 'Gestor de Risco'
        list_lookups = 'pdi__instituicao',
        select_display = 'descricao', 'pdi',

    def __str__(self):
        return self.descricao

    @meta('Instituição')
    def get_instituicao(self):
        return self.pdi.instituicao


class ObjetivoEstrategico(models.Model):
    plano_gestao = models.ForeignKey(PlanoGestao, verbose_name='Plano Estratégico', null=True, example='UFRN', filter=('pdi', 'pk'))
    descricao = models.CharField(verbose_name='Objetivo Estratégico', search=True, example='Diminuir evasão escolar')

    fieldsets = (
        ('Dados Gerais', {'fields': ('plano_gestao', 'descricao',)}),
    )

    class Meta:
        verbose_name = 'Objetivo Estratégico'
        verbose_name_plural = 'Objetivos Estratégicos'
        menu = '5 Objetivos Estratégicos', 'fa-dot-circle-o'
        list_display = 'get_pdi', 'plano_gestao', 'descricao',
        can_admin_by_organization = 'Facilitador', 'Administrador'
        can_view_by_organization = 'Servidor', 'Gestor de Risco'
        can_list_by_organization = 'Servidor', 'Gestor de Risco'
        list_lookups = 'plano_gestao__pdi__instituicao',

    def __str__(self):
        return self.descricao

    @meta('PDI')
    def get_pdi(self):
        return self.plano_gestao.pdi

    @meta('Instituição')
    def get_instituicao(self):
        return self.plano_gestao.pdi.instituicao


class Subprocesso(models.Model):
    processo = models.ForeignKey(Processo, verbose_name='Processo', filter=('macroprocesso', 'id'), example='Otimizar o ensino')
    descricao = models.CharField(verbose_name='Subprocesso', search=True, example='Capacitar Professores')
    objetivo = models.TextField(verbose_name='Objetivo', null=True, blank=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('processo', 'descricao', 'objetivo')}),
        ('Fluxogramas', {'relations': ('fluxograma_set',)}),
    )

    class Meta:
        verbose_name = 'Subprocesso'
        verbose_name_plural = 'Subprocessos'
        menu = '4 Cadeia de Valor::Subprocessos', 'fa-sitemap'
        list_display = 'get_macroprocesso', 'processo', 'descricao'
        can_admin_by_organization = 'Facilitador', 'Administrador'
        can_view_by_organization = 'Servidor', 'Gestor de Risco'
        can_list_by_organization = 'Servidor', 'Gestor de Risco'
        list_lookups = 'processo__macroprocesso__instituicao',
        class_diagram = 'processo', 'macroprocesso', 'classificacaomacroprocesso'

    @meta('Macroprocesso')
    def get_macroprocesso(self):
        return self.processo.macroprocesso

    def __str__(self):
        return self.descricao


class ObjetivoOperacional(Subprocesso):

    class Meta:
        verbose_name = 'Objetivo Operacional'
        verbose_name_plural = 'Objetivos Operacionais'
        list_display = 'get_macroprocesso', 'processo', 'descricao'
        can_admin_by_organization = 'Facilitador', 'Administrador'
        can_view_by_organization = 'Gestor de Risco'
        proxy = True
        usecase = 15


VALORES_PROBABILIDADE = {'Muito Baixo': 1, 'Baixo': 2, 'Moderado': 3, 'Alto': 4, 'Muito Alto': 5}
VALORES_IMPACTO = {'Insignificante': 1, 'Pequeno': 2, 'Médio': 3, 'Grande': 4, 'Crítico': 5}


class EventoManager(models.DefaultManager):

    @subset('Risco Inerente Não-Classificado')
    def risco_interente_nao_classificado(self):
        return self.filter(risco__isnull=True)

    @subset('Resposta ao Risco Inerente Pendente')
    def sem_resposta_risco_inerente(self):
        return self.filter(risco__isnull=False, resposta_risco__isnull=True)

    @subset('Atividades de Controle Não-Definidas ')
    def planos_nao_definidos(self):
        return self.filter(atividades_pendentes=True)

    @subset('Risco Residual Não-Classificado')
    def risco_residual_nao_classificado(self):
        qs = self.filter(risco_residual__isnull=True).exclude(atividades_pendentes=True)
        qs1 = qs.filter(resposta_risco__descricao__in=('Aceitar', 'Evitar'))
        qs2 = qs.filter(resposta_risco__descricao__in=('Transferir', 'Mitigar')).exclude(planoacao__isnull=True, planocontigencia__isnull=True, atividades_mitigadoras_associadas__isnull=True)
        return qs1 | qs2

    @meta('Eventos por Resposta ao Risco', can_view=('Facilitador', 'Gestor de Risco', 'Servidor'), dashboard='right', formatter='donut_chart')
    def get_qtd_por_resposta_risco(self):
        return self.all(self._user).count('resposta_risco')

    @meta('Eventos por Tipo de Risco', can_view=('Facilitador', 'Gestor de Risco', 'Servidor'), dashboard='center', formatter='bar_chart')
    def get_qtd_por_tipo_risco(self):
        return self.all(self._user).count('tipos_risco')

    @meta('Eventos por Grau de Risco Inerente', can_view=('Facilitador', 'Gestor de Risco', 'Servidor'), dashboard='center', formatter='box_chart')
    def get_qtd_por_grau_risco_inerente(self):
        return self.all(self._user).count('risco')

    @meta('Eventos por Grau de Risco Residual', can_view=('Facilitador', 'Gestor de Risco', 'Servidor'), dashboard='center', formatter='box_chart')
    def get_qtd_por_grau_risco_residual(self):
        return self.all(self._user).count('risco_residual')


class Evento(models.Model):
    PROBABILIDADE_CHOICES = [['Muito Baixo', 'Muito Baixo'], ['Baixo', 'Baixo'], ['Moderado', 'Moderado'],
                             ['Alto', 'Alto'], ['Muito Alto', 'Muito Alto']]

    GRAU_RISCO_CHOICE = [['Baixo', 'Baixo'], ['Moderado', 'Moderado'], ['Alto', 'Alto'], ['Muito Alto', 'Muito Alto']]

    IMPACTO_CHOICES = [['Insignificante', 'Insignificante'], ['Pequeno', 'Pequeno'], ['Médio', 'Médio'],
                       ['Grande', 'Grande'], ['Crítico', 'Crítico']]

    NIVEL_CONFIANCA_CHOICES = [['Inexistente', 'Inexistente'],
                              ['Fraco', 'Fraco'],
                              ['Mediano', 'Mediano'],
                              ['Satisfatório', 'Satisfatório'],
                              ['Forte', 'Forte']
                              ]

    gerenciador = models.ForeignKey(Facilitador, verbose_name='Facilitador', null=True, filter=True, example='1111111')
    cadastrador = models.ForeignKey(GestorRisco, verbose_name='Gestor de Risco', null=True, filter=('pk', 'unidade'), example='2222222')
    tipos_risco = models.ManyToManyField(TipoRisco, verbose_name='Tipos de Risco', example='Pessoal')
    descricao = models.TextField(verbose_name='Evento', null=True, search=True, example='Falta de recursos')
    causas = models.TextField('Causas', null=True, blank=True, exclude=True, help_text='O que faz com que o evento aconteça.', example='Contigenciamento')
    efeito = models.TextField('Efeitos', null=True, blank=True, exclude=True, help_text='A consequência da materizalização do evento (risco).', example='Corte nas despesas')
    probabilidade = models.CharField(verbose_name='Probabilidade', choices=PROBABILIDADE_CHOICES, null=True, exclude=True, example='Moderado', help_text='Muito Baixa: Evento extraordinário.\nBaixa: Evento casual, inesperado. Existe histórico de ocorrência.\nModerada: Evento esperado de frequência reduzida. Histórico parcialmente conhecido.\nAlta: Evento usual de frequência habitual. Histórico amplamente conhecido.\nMuito Alta: Evento que se repete seguidamente. Interfere no ritmo das atividades.')
    impacto = models.CharField(verbose_name='Impacto', choices=IMPACTO_CHOICES, null=True, exclude=True, example='Alto', help_text='Insignificante: Não afeta os objetivos. \nPequeno: Pouco afeta os objetivos.\nMédio: Torna incerto ou duvidoso o alcance do objetivo.\nGrande: Torna improvável o alcance do objetivo.\nCrítico: Capaz de impedir o alcance do objetivo.')
    risco = models.CharField('Risco Inerente', null=True, exclude=True, filter=True, choices=GRAU_RISCO_CHOICE)
    resposta_risco = models.ForeignKey(RespostaRisco, verbose_name='Resposta ao Risco', null=True, blank=True, exclude=True, filter=True, example='Mitigar', help_text='\nAceitar: O gestor abre mão de qualquer atividade de controle preventiva. Em alguns casos, é necessário o estabelecimento de atividades de controle mitigadora. Essa resposta geralmente é escolhida quando os riscos são baixos ou bastante improváveis.\n\nEvitar: O gestor decide descontinuar a atividade que geraria o risco. Essa resposta deve ser escolhida quando o gestor puder abrir mão do objetivo ao qual se pretende alcançar.\n\nMitigar: São adotadas medidas para reduzir a probabilidade do evento e/ou do seu impacto, caso ele venha a se materializar.\n\nTransferir: Transferir o risco significa atribuir a outrem sua responsabilidade. As principais formas de se transferir o risco ocorrem por meio da terceirização ou da contratação de seguros.')
    atividades_pendentes = models.BooleanField(verbose_name='Atividades de Controle Pendentes', default=False, exclude=True)
    nivel_confianca = models.CharField(verbose_name='Nível de Confiança', choices=NIVEL_CONFIANCA_CHOICES, null=True, exclude=True, example='Mediano', blank=True, help_text='\nInexistente: Controles inexistentes, mal desenhados ou mal implementados, isto é, não funcionais.\n\nFraco: Controles têm abordagens ad hoc, tendem a ser aplicados caso a caso, a responsabilidade é individual, havendo elevado grau de confiança no conhecimento das pessoas.\n\nMediano: Controles implementados mitigam alguns aspectos do risco, mas não contemplam todos os aspectos relevantes do risco devido a deficiências no desenho ou nas ferramentas utilizadas.\n\nSatisfatório: Controles implementados e sustentados por ferramentas adequadas e, embora passíveis de aperfeiçoamento, mitigam o risco satisfatoriamente.\n\nForte: Controles implementados podem ser considerados a "melhor prática", mitigando todos os aspectos relevantes do risco.\n\n')
    criterio_risco_residual = models.TextField('Critério de Probabilidade', null=True, blank=True, example='Histórico do orçamento', help_text='Critério utilizado pelo gestor para definir a probabilidade do evento ocorrer.', exclude=True)
    probabilidade_residual = models.CharField(verbose_name='Probabilidade Residual', choices=PROBABILIDADE_CHOICES, null=True, exclude=True, example='Baixo')
    impacto_residual = models.CharField(verbose_name='Impacto Residual', choices=IMPACTO_CHOICES, null=True, exclude=True, example='Pequeno')
    risco_residual = models.CharField('Risco Residual', null=True, exclude=True, filter=True, choices=GRAU_RISCO_CHOICE)

    atividades_mitigadoras_associadas = models.ManyToManyField('gerifes.PlanoAcao', blank=True, verbose_name='Outras Atividades Preventivas', exclude=True, related_name='associacao_set', add_label='Adicionar Atividade Preventiva já Cadastrada')

    validado = models.NullBooleanField(verbose_name='Validado', filter=True, exclude=True)
    critico = models.BooleanField(verbose_name='Risco Crítico', blank=True, filter=True)

    class Meta:
        verbose_name = 'Evento'
        verbose_name_plural = 'Eventos'
        icon = 'fa-retweet'
        can_admin_by_organization = 'Administrador', 'Facilitador'
        can_list_by_organization = 'Facilitador', 'Servidor'
        can_list_by_unit = 'Gestor de Risco'
        list_lookups = 'cadastrador__unidade__instituicao', 'cadastrador__unidade'


    def __str__(self):
        return self.descricao

    @meta('Risco Inerente', formatter='dados_risco_inerente')
    def get_dados_risco_inerente(self):
        return self.risco, self.probabilidade, self.impacto

    @meta('Risco Residual', formatter='dados_risco_residual')
    def get_dados_risco_residual(self):
        return self.risco_residual, self.nivel_confianca

    @action('Classificar Risco Inerente', can_execute_by_organization='Administrador', can_execute_by_role='Facilitador', subset='risco_interente_nao_classificado', condition='not risco', usecase=18)
    def classificar_risco_inerente(self, causas, efeito, probabilidade, impacto):
        self.probabilidade = probabilidade
        self.impacto = impacto
        self.calcular_risco()
        self.probabilidade_residual = self.probabilidade
        self.impacto_residual = self.impacto
        self.risco_residual = None
        self.save()

    @action('Alterar Classificação do Risco Inerente', category='Ações', condition='risco', can_execute_by_organization='Administrador', can_execute_by_role='Facilitador', usecase=19)
    def alterar_classificacao_risco_inerente(self, causas, efeito, probabilidade, impacto):
        return self.classificar_risco_inerente(causas, efeito, probabilidade, impacto)

    @action('Resposta ao Risco Inerente', category='Ações', subset='sem_resposta_risco_inerente', condition='risco', can_execute_by_organization='Administrador', can_execute_by_role='Facilitador', usecase=20)
    def resposta_ao_risco(self, resposta_risco):
        self.resposta_risco = resposta_risco
        if self.resposta_risco.descricao == 'Aceitar':
            self.classificar_risco_residual('Inexistente')
        self.save()

    def requer_cadastro_atividades_controle(self):
        return Evento.objects.filter(pk=self.pk).filter(planoacao__isnull=True, planocontigencia__isnull=True, atividades_mitigadoras_associadas__isnull=True, resposta_risco__isnull=False).exclude(resposta_risco__descricao__in=('Aceitar', 'Evitar'))

    def pode_finalizar_cadastro_atividades_controle(self):
        return self.atividades_pendentes and not self.requer_cadastro_atividades_controle()

    @action('Finalizar Atividades de Controle', category='Ações', subset='planos_nao_definidos', condition='pode_finalizar_cadastro_atividades_controle', can_execute_by_organization='Administrador', can_execute_by_role='Facilitador', usecase=23)
    def finalizar_cadastro_atividades_controle(self):
        self.atividades_pendentes = False
        self.save()

    @action('Classificar Risco Residual', category='Ações', subset='risco_residual_nao_classificado', condition='not risco_residual', can_execute_by_organization='Administrador', can_execute_by_role='Facilitador', usecase=24)
    def classificar_risco_residual(self, nivel_confianca):
        if nivel_confianca:
            self.nivel_confianca = nivel_confianca
            self.probabilidade_residual = self.probabilidade
            self.impacto_residual = self.impacto
            self.calcular_risco_residual()
        else:
            self.nivel_confianca = None
            self.probabilidade_residual = None
            self.impacto_residual = None
            self.risco_residual = None
        self.save()

    @action('Alterar Classificação do Risco Residual', category='Ações', condition='risco_residual', can_execute_by_organization='Administrador', can_execute_by_role='Facilitador', usecase=25)
    def alterar_classificacao_risco_residual(self, nivel_confianca):
        return self.classificar_risco_residual(nivel_confianca)

    def save(self):
        if not self.probabilidade_residual or not self.impacto_residual:
            self.probabilidade_residual = self.probabilidade
            self.impacto_residual = self.impacto
        super(Evento, self).save()
        if self.requer_cadastro_atividades_controle():
            self.atividades_pendentes = True
        else:
            self.atividades_pendentes = False
        super(Evento, self).save()

    def calcular_risco(self):
        if self.probabilidade and self.impacto:
            produto = VALORES_PROBABILIDADE[self.probabilidade] * VALORES_IMPACTO[self.impacto]
        else:
            produto = 0

        if 2 >= produto >= 1:
            self.risco = 'Baixo'
        elif 6 >= produto >= 3:
            self.risco = 'Moderado'
        elif 12 >= produto >= 8:
            self.risco = 'Alto'
        elif 25 >= produto >= 15:
            self.risco = 'Muito Alto'

    def calcular_risco_residual(self):
        if self.probabilidade_residual and self.impacto_residual:
            produto = VALORES_PROBABILIDADE[self.probabilidade_residual] * VALORES_IMPACTO[self.impacto_residual]
        else:
            produto = 0

        if self.nivel_confianca == 'Inexistente':
            produto = produto * 1
        elif self.nivel_confianca == 'Fraco':
            produto = produto * 0.8
        elif self.nivel_confianca == 'Mediano':
            produto = produto * 0.6
        elif self.nivel_confianca == 'Satisfatório':
            produto = produto * 0.4
        elif self.nivel_confianca == 'Forte':
            produto = produto * 0.2

        if 2.9 >= produto >= 0.1:
            self.risco_residual = 'Baixo'
        elif 7.9 >= produto >= 3:
            self.risco_residual = 'Moderado'
        elif 14.9 >= produto >= 8:
            self.risco_residual = 'Alto'
        elif 25 >= produto >= 15:
            self.risco_residual = 'Muito Alto'


class EventoOperacionalManager(EventoManager):

    @action('Validar', can_execute=('Facilitador', 'Administrador'))
    def validar(self):
        self.update(validado=True)

    @action('Cancelar Validação', can_execute=('Facilitador', 'Administrador'))
    def cancelar_validacao(self):
        self.update(validado=False)


class EventoOperacional(Evento):
    subprocesso = models.ForeignKey(Subprocesso, null=True, verbose_name='Subprocesso', blank=True, filter=('processo__macroprocesso', 'processo', 'pk'), example='Capacitar Professores')

    class Meta:
        verbose_name = 'Evento Operacional'
        verbose_name_plural = 'Riscos dos Objetivos Operacionais'
        list_display = 'subprocesso__processo__macroprocesso', 'subprocesso__processo', 'subprocesso', 'cadastrador', 'descricao', 'get_dados_risco_inerente', 'resposta_risco', 'get_dados_risco_residual', 'validado'
        select_related = 'cadastrador__unidade', 'gerenciador__servidor__instituicao', 'subprocesso__processo__macroprocesso', 'resposta_risco'
        icon = 'fa-retweet'
        list_shortcut = True
        can_admin_by_organization = 'Administrador', 'Facilitador'
        can_list_by_organization = 'Facilitador', 'Servidor'
        can_list_by_unit = 'Gestor de Risco'
        pdf = True
        list_xls = 'subprocesso__processo__macroprocesso', 'subprocesso__processo', 'subprocesso', 'cadastrador', 'descricao', 'causas', 'efeito', 'risco', 'resposta_risco', 'risco_residual', 'validado'
        order_by = 'subprocesso__processo__macroprocesso', 'subprocesso__processo', 'subprocesso', 'pk'
        list_lookups = 'cadastrador__unidade__instituicao', 'cadastrador__unidade'
        usecase = 17.1
        class_diagram = 'evento', 'tiporisco', 'respostarisco', 'subprocesso'

    fieldsets = (('Dados Gerais', {'fields': (('gerenciador','cadastrador'), 'tipos_risco', 'subprocesso', 'descricao', 'critico')}),
                 ('Riscos::Risco Inerente', {'fields': ('causas', 'efeito', 'probabilidade', 'impacto', ('risco', 'get_dados_risco_inerente'), 'resposta_risco')}),
                 ('Riscos::Risco Residual', {'fields': ('nivel_confianca', ('risco_residual', 'get_dados_risco_residual'))}),
                 ('Atividades Preventivas::Atividades Preventivas', {'relations': ('planoacao_set',)}),
                 ('Atividades Preventivas::Atividades Preventivas Associadas', {'relations': ('atividades_mitigadoras_associadas',)}),
                 ('Planos de Contingência::Planos de Contingência', {'relations': ('planocontigencia_set',)}),
                 )


class EventoEstrategicoManager(EventoManager):

    @action('Validar', can_execute=('Facilitador', 'Administrador'))
    def validar(self):
        self.update(validado=True)

    @action('Cancelar Validação', can_execute=('Facilitador', 'Administrador'))
    def cancelar_validacao(self):
        self.update(validado=False)


class EventoEstrategico(Evento):
    objetivo_estrategico = models.ForeignKey(ObjetivoEstrategico, null=True, verbose_name='Objetivo Estratégico', blank=True, filter=('plano_gestao__pdi', 'plano_gestao', 'pk'), example='Diminuir Evasão Escolar')

    class Meta:
        verbose_name = 'Evento Estratégico'
        verbose_name_plural = 'Riscos dos Objetivos Estratégicos'
        list_display = 'objetivo_estrategico__plano_gestao__pdi', 'objetivo_estrategico__plano_gestao', 'objetivo_estrategico', 'cadastrador', 'descricao', 'get_dados_risco_inerente', 'resposta_risco', 'get_dados_risco_residual', 'validado'
        select_related = 'cadastrador__unidade', 'gerenciador__servidor__instituicao', 'objetivo_estrategico__plano_gestao__pdi', 'resposta_risco'
        icon = 'fa-retweet'
        list_shortcut = True
        can_admin_by_organization = 'Administrador', 'Facilitador'
        can_list_by_organization = 'Facilitador', 'Servidor'
        can_list_by_unit = 'Gestor de Risco'
        pdf = True
        list_xls = 'objetivo_estrategico__plano_gestao__pdi', 'objetivo_estrategico__plano_gestao', 'objetivo_estrategico', 'cadastrador', 'descricao', 'causas', 'efeito', 'risco', 'resposta_risco', 'risco_residual', 'validado'
        order_by = 'objetivo_estrategico__plano_gestao__pdi', 'objetivo_estrategico__plano_gestao', 'objetivo_estrategico', 'pk'
        list_lookups = 'cadastrador__unidade__instituicao', 'cadastrador__unidade'
        usecase = 17.2
        class_diagram = 'evento', 'tiporisco', 'respostarisco', 'subprocesso'

    fieldsets = (('Dados Gerais', {'fields': (('gerenciador','cadastrador'), 'tipos_risco', 'objetivo_estrategico', 'descricao', 'critico')}),
                 ('Riscos::Risco Inerente', {'fields': ('causas', 'efeito', 'probabilidade', 'impacto', ('risco', 'get_dados_risco_inerente'), 'resposta_risco')}),
                 ('Riscos::Risco Residual', {'fields': ('nivel_confianca', ('risco_residual', 'get_dados_risco_residual'))}),
                 ('Atividades Preventivas::Atividades Preventivas', {'relations': ('planoacao_set',)}),
                 ('Atividades Preventivas::Atividades Preventivas Associadas', {'relations': ('atividades_mitigadoras_associadas',)}),
                 ('Planos de Contingência::Planos de Contingência', {'relations': ('planocontigencia_set',)}),
                 )


class PlanoAcaoManager(models.DefaultManager):

    @meta('Atividades Preventivas por Status', can_view=('Facilitador', 'Gestor de Risco', 'Servidor'), dashboard='right', formatter='donut_chart')
    def get_total_por_status(self):
        return self.count('status')

    @subset('Atrasadas', can_view='Gestor de Risco', can_alert=True)
    def atrasadas(self):
        return self.filter(data_prevista_conclusao__lt=datetime.date.today()).exclude(status=PlanoAcao.CONCLUIDO)

    def no_prazo(self):
        return self.filter(data_prevista_conclusao__gte=datetime.date.today()).exclude(status=PlanoAcao.CONCLUIDO)

    @meta('Implementando Atividades Preventivas', can_view=('Facilitador', 'Gestor de Risco', 'Servidor'), dashboard='right', formatter='donut_chart')
    def por_status(self):
        statistics = QueryStatistics('Implementando Atividades Preventivas')
        statistics.add('Atrasadas', PlanoAcao.objects.all(self._user).atrasadas())
        statistics.add('No Prazo', PlanoAcao.objects.all(self._user).no_prazo())
        return statistics


class PlanoAcao(models.Model):
    NAO_INCIADO = 'Não Iniciado'
    INICIADO = 'Iniciado'
    CONCLUIDO = 'Concluído'

    STATUS_CHOICES = [[CONCLUIDO, CONCLUIDO], [INICIADO, INICIADO], [NAO_INCIADO, NAO_INCIADO]]

    evento = models.ForeignKey(Evento, verbose_name='Evento', filter=('cadastrador__unidade', 'cadastrador', 'id'), example='Falta de recursos')
    descricao = models.CharField(verbose_name='Atividade Preventiva', search=True, example='Otimizar os gastos')
    data_prevista_conclusao = models.DateField(verbose_name='Previsão Conclusão', null=True)
    status = models.CharField(verbose_name='Status', choices=STATUS_CHOICES, filter=True, example='Iniciado')
    detalhamento = models.TextField(verbose_name='Detalhamento', null=True, blank=True, example='...')
    eficaz = models.NullBooleanField(verbose_name='Efetividade', filter=True)

    fieldsets = (
        ('Evento', {'relations': ('evento',)}),
        ('Dados Gerais', {'fields': ('descricao', ('data_prevista_conclusao', 'status'), 'detalhamento')}),
        ('Status da Atividade', {'fields': ('eficaz',)}),
    )

    class Meta:
        verbose_name = 'Atividade Preventiva'
        verbose_name_plural = 'Atividades Preventivas'
        menu = '6 Gerenciamento::Atividades Preventivas', 'fa-line-chart'
        can_admin_by_organization = 'Facilitador', 'Administrador'
        can_list_by_unit = 'Gestor de Risco'
        can_list_by_organization = 'Facilitador'
        can_view_by_organization = 'Servidor'
        list_lookups = 'evento__cadastrador', 'evento__cadastrador__unidade', 'evento__cadastrador__unidade__instituicao'
        list_display = 'evento', 'descricao', 'evento__get_dados_risco_inerente', 'data_prevista_conclusao', 'status', 'eficaz', 'detalhamento'
        usecase = 21
        class_diagram = 'planocontigencia', 'evento'
        order_by = '-status',

    def __str__(self):
        return self.descricao

    def save(self):
        if self.eficaz:
            if not self.status == PlanoAcao.CONCLUIDO:
                raise ValidationError('A efetividade só pode ser confirmada para planos concluídos')
        if self.eficaz is None and self.status == PlanoAcao.CONCLUIDO:
                raise ValidationError('Informe a efetividade da atividade')
        super(PlanoAcao, self).save()

    def delete(self):
        super(PlanoAcao, self).delete()
        self.evento.save()

    def usuario_pode_atualizar(self):
        return self.evento.cadastrador.servidor.login == self._user.username

    @action('Atualizar', can_execute_by_role='Gestor de Risco', inline=True, condition='usuario_pode_atualizar')
    def atualizar(self, status, detalhamento, eficaz):
        self.status = status
        self.detalhamento = detalhamento
        self.eficaz = eficaz
        self.save()


class PlanoContigencia(models.Model):
    evento = models.ForeignKey(Evento, verbose_name='Evento', example='Otimizar os gastos', filter=('cadastrador__unidade', 'cadastrador', 'id'))
    descricao = models.CharField(verbose_name='Plano de Contingência', search=True, example='Otimizar os gastos')

    class Meta:
        verbose_name = 'Plano de Contingência'
        verbose_name_plural = 'Planos de Contingência'
        menu = '6 Gerenciamento::Planos de Contingência', 'fa-line-chart'
        can_admin_by_organization = 'Facilitador', 'Administrador'
        can_view = 'Servidor'
        can_view_by_unit = 'Gestor de Risco'
        can_edit_by_role = 'Gestor de Risco'
        can_list_by_organization = 'Facilitador'
        list_lookups = 'evento__cadastrador', 'evento__cadastrador__unidade', 'evento__cadastrador__unidade__instituicao'
        list_display = 'evento', 'descricao'
        usecase = 22

    fieldsets = (
        ('Evento', {'relations': ('evento',)}),
        ('Dados Gerais', {'fields': ('descricao',)}),
    )

    def __str__(self):
        return self.descricao

    def save(self):
        super(PlanoContigencia, self).save()

    def delete(self):
        super(PlanoContigencia, self).delete()
        self.evento.save()


class Arquivo(models.Model):
    NORMAS = 'Normativos'
    MODELOS = 'Modelos de Gestão de Riscos'
    TRABALHOS = 'Trabalhos Científicos'
    CURSOS = 'Cursos e Manuais'
    OUTROS = 'Outras Publicações'

    CATEGORIA_CHOICES = [[NORMAS, NORMAS], [MODELOS, MODELOS], [TRABALHOS, TRABALHOS], [CURSOS, CURSOS], [OUTROS, OUTROS]]

    categoria = models.CharField(verbose_name='Categoria', filter=True, choices=CATEGORIA_CHOICES)
    nome = models.CharField(verbose_name='Nome', search=True)
    arquivo = models.FileField(verbose_name='Arquivo Digitalizado', upload_to='acervo')

    class Meta:
        verbose_name = 'Arquivo'
        verbose_name_plural = 'Arquivos'
        menu = '2 Biblioteca::Arquivos', 'fa-book'


class Fluxograma(models.Model):
    subprocesso = models.ForeignKey(Subprocesso, null=True, verbose_name='Subprocesso', blank=False, filter=True, composition=True, example='Capacitar Professores')
    arquivo = models.FileField(verbose_name='Arquivo', upload_to='fluxogramas', null=True, formatter='pdf_icon')

    class Meta:
        verbose_name = 'Fluxograma'
        verbose_name_plural = 'Fluxogramas'
        menu = 'Cadastro::Fluxogramas'
        can_admin_by_organization = 'Facilitador', 'Administrador'
        can_list_by_organization = 'Gestor de Risco', 'Servidor'
        list_lookups = 'subprocesso__processo__macroprocesso__instituicao',
        list_shortcut = True
        icon = 'fa-share-alt'
        usecase = 16

    def __str__(self):
        return self.arquivo.name
