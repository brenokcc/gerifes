# -*- coding: utf-8 -*-
from djangoplus.decorators.formatters import formatter
from django import template
register = template.Library()


@formatter()
def pdf_icon(url):
    return '<a target="_blank" href="/media/{}"><i class="fa fa-file-pdf-o"></i> Visualizar Arquivo</a>'.format(url)


@register.filter
@formatter()
def dados_risco_inerente(dados_risco, **kwargs):
    risco, probabilidade, impacto = dados_risco
    if risco:
        if risco == u'Baixo':
            cor = u'#70DB93'
        elif risco == u'Moderado':
            cor = u'#E6E675'
        elif risco == u'Alto':
            cor = u'orange'
        elif risco == u'Muito Alto':
            cor = u'red'
        return u'<span style="background-color:%s; cursor:help;" title="Probabilidade: %s / Impacto: %s">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>' % (cor, probabilidade, impacto) + u'<span style="color:%s">&nbsp;&nbsp;%s</span>' % (cor, risco)
    else:
        return u'<div>Não-Classificado</div>'


@register.filter
@formatter()
def dados_risco_residual(dados_risco, **kwargs):
    risco, nivel_confianca = dados_risco
    if risco:
        if risco == u'Baixo':
            cor = u'#70DB93'
        elif risco == u'Moderado':
            cor = u'#E6E675'
        elif risco == u'Alto':
            cor = u'orange'
        elif risco == u'Muito Alto':
            cor = u'red'
        return u'<span style="background-color:%s; cursor:help;" title="Nível de Confiança: %s">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>' % (cor, nivel_confianca) + u'<span style="color:%s">&nbsp;&nbsp;%s</span>' % (cor, risco)
    else:
        return u'<div>Não-Classificado</div>'
